import sys
import pandas as pd
import argparse as ap
import re

parser = ap.ArgumentParser(description='This is a program to build counts & frequency tables from a data table in .csv, .xls/.xlsx, or .txt/.tsv/.tab format.')
file = parser.add_argument_group('file')
file.add_argument('file', metavar='file_path', type=str, nargs=1, help='Path to first file to be included in the report')

parser.add_argument('rowfield', type=str, help='Field name containing the ROW values for the count table')
parser.add_argument('colfield', type=str, help='Field name containing the primary COLUMN values for the count table')
parser.add_argument('colfield2', type=str, nargs='?', help='(OPTIONAL) Field name containing the secondary COLUMN values for the count table. If provided, the values in the primary column will be further subset by the values in this column.')

args = parser.parse_args()

# Parse file name to get name w/o extension
FNAMEPATTERN = '(.*)(\.xlsx|\.xls|\.csv|\.tab|\.tsv|\.txt)$'
def parseFile(fpath):
    fname = fpath.split('/').pop()
    result = re.match(FNAMEPATTERN, fname)
    fname = result.group(1)
    ext = result.group(2)
    if ext == '.xlsx' or ext == '.xls':
        df = pd.read_excel(fpath)
    elif ext == '.csv':
        df = pd.read_csv(fpath)
    elif ext == '.tab' or ext == '.tab' or ext == '.txt':
        df = pd.read_csv(fpath, sep='\t')
    return (df, fname)

(df, dfname) = parseFile(args.file[0])

rowfield=args.rowfield
colfield=args.colfield
if args.colfield2:
  colfield2=args.colfield2
else:
  colfield2=None

# Make the counts & frequencies tables & write them out
if colfield2 is None:
  countdf=pd.crosstab(index=df[rowfield],columns=df[colfield],margins=True)
  freqdf=pd.crosstab(index=df[rowfield],columns=df[colfield],normalize=True,margins=True)
  countdf.to_csv(dfname+'_COUNTS'+colfield+'by'+rowfield+'.txt',sep='\t',index=True)
  freqdf.to_csv(dfname+'_FREQ'+colfield+'by'+rowfield+'.txt',sep='\t',index=True)
else:
  countdf=pd.crosstab(index=df[rowfield],columns=[df[colfield],df[colfield2]],margins=False)
  freqdf=pd.crosstab(index=df[rowfield],columns=[df[colfield],df[colfield2]],normalize=True,margins=False)
  countdf.to_csv(dfname+'_COUNTS'+colfield+'-and-'+colfield2+'by'+rowfield+'.txt',sep='\t',index=True)
  freqdf.to_csv(dfname+'_FREQ'+colfield+'-and-'+colfield2+'by'+rowfield+'.txt',sep='\t',index=True)
