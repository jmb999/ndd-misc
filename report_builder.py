import sys
import pandas as pd
import argparse as ap
import re

parser = ap.ArgumentParser(description='This is a program to build composite reports from NDD database exports by performing SQL-style joins on two reports exported from either REDCap or LabKey in .csv, .tab, or .xls/.xlsx format. The user must specify the type of join operation to be performed (default is to inner join on common IDs) and the names of the key columns in each file.')
file1 = parser.add_argument_group('file1')
file1.add_argument('file1', metavar='file1_path', type=str, nargs=1, help='Path to first file to be included in the report')
file1.add_argument('file1_index', metavar='file1_index', type=str, nargs=1, help='Index column in file 1 to join on')
file2 = parser.add_argument_group('file2')
file2.add_argument('file2', metavar='file2_path', type=str, nargs=1, help='Path to first file to be included in the report')
file2.add_argument('file2_index', metavar='file2_index', type=str, nargs=1, help='Index column in file 2 to join on')

parser.add_argument('outfile', type=str, nargs='?', default='', help='(Optional) Name of output file. Default is \'./[file1 name]_[file2_name].csv\'.')
parser.add_argument('-d','--drop', action='store_true', default=False, help='Drop rows in either data frame where the index column is blank. Default behavior is to retain all rows.')

joingroup = parser.add_mutually_exclusive_group(required=False)
joingroup.add_argument('-l','--left', action='store_true', default=False, help='Left join files (i.e. include all rows from file 1)')
joingroup.add_argument('-i','--inner', action='store_true', default=True, help='(Default) Inner join files (i.e. include only rows in common between files 1 and 2)')
joingroup.add_argument('-r','--right', action='store_true', default=False, help='Right join files (i.e. include all rows from file 2)')
joingroup.add_argument('-o','--outer', action='store_true', default=False, help='Outer join files (i.e. include all rows from both files)')

args = parser.parse_args()

# Parse file name to get name w/o extension
FNAMEPATTERN = '(.*)(\.xlsx|\.xls|\.csv|\.tab|\.tsv)$'
def parseFile(fpath):
    fname = fpath.split('/').pop()
    result = re.match(FNAMEPATTERN, fname)
    fname = result.group(1)
    ext = result.group(2)
    if ext == '.xlsx' or ext == '.xls':
        df = pd.read_excel(fpath)
    elif ext == '.csv':
        df = pd.read_csv(fpath)
    elif ext == '.tab' or ext == '.tsv':
        df = pd.read_csv(fpath, sep='\t')
    return (df, fname)

(f1, f1name) = parseFile(args.file1[0])
(f2, f2name) = parseFile(args.file2[0])

# Set join type
if args.right:
    jointype = 'right'
elif args.left:
    jointype = 'left'
elif args.outer:
    jointype = 'outer'
elif args.inner:
    jointype = 'inner'
else:
    sys.exit('ERROR: Invalid join type specified')

# If --drop flag was passed, drop all rows where index column is empty
if args.drop:
    f1.dropna(subset=args.file1_index, inplace=True)
    f2.dropna(subset=args.file2_index, inplace=True)

# Merge reports and write out the results
joined = f1.merge(f2, how=jointype, left_on=args.file1_index[0], right_on=args.file2_index[0],suffixes=('_f1','_f2'))
if args.outfile == '':
    outname = f1name + '_' + f2name + '.csv'
else:
    outname = args.outfile

joined.to_csv(outname,index=False)
